import numpy as np
from scipy.optimize import minimize
import scipy.linalg as sl
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt


class NewtonMethod:
    def __init__(self, problem):
        self.problem = problem
        pass

    def solve(self):
        gra = self.problem.gradient
        n = self.problem.dimensions
        xk = np.array([-0.3, -0.8])  # initial guess
        steps = [np.copy(xk)]
        k = 0
        while not np.allclose(gra(xk), [0] * n):
            k += 1
            """
            Gk = self.hessian(gra, xk, n)
            try:
                cho = sl.cho_factor(Gk)
                alpha = 1
                x = sl.cho_solve(cho, alpha * gra(xk))
                
            except sl.LinAlgError:
                raise sl.LinAlgError('The Hessian of the objective function is not positive definite in', xk) 
            xk = xk - x
            
            """
            Gk = self.hessian(gra, xk, n)
            Hk = np.linalg.inv(Gk)
            dir_k = -np.dot(Hk, gra(xk))
            alpha = self.exactLineSearch(xk, dir_k)
            # alpha = self.inexactLineSearch(xk, dir_k)
            xk += alpha * dir_k


            steps.append(np.copy(xk))
            if k > 10000:
                print('did not converge')
                break
        return steps


    def hessian(self, gradient,x,n,H=1e-6):
        G=np.zeros([n,n])
        for j in range(n):
            ej=np.zeros(np.shape(x))
            ej[j]=1
            h=H*x[j]
            if h == 0:
                h = H
            G1 = (gradient(x + h * ej) - gradient(x)) / (h)
            G[:,j]=G1
        G=(G+np.transpose(G))/2
        return G


    def noLineSearch(self):
        return 1

    def exactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a * sk)
        res = minimize(fAlpha, np.array([1]))
        return res.x

    def inexactLineSearch(self, xk, sk):
        fAlpha = lambda a: self.problem.objFunc(xk + a *sk)
        fPrimAlpha = lambda a: np.dot(self.problem.gradient(xk + a*sk), sk)
        rho = 0.1
        #sigma = 0.7 # used in a different variation of LC and RC
        tau = 0.1
        chi = 9.0
        alpha0 = 1 #guess
        alphaL = 0
        alphaU = 10e99
        LC = False
        RC = False
        while not (LC and RC):
            #print(alpha0)
            #print(alphaL)
            #print(alphaU)
            if alphaU == alphaL:
                break
            if not LC:
                if alphaL  == alpha0:
                    deltaAlpha0 = 0
                else:
                    deltaAlpha0 = (alpha0 - alphaL)*fPrimAlpha(alpha0)/(fPrimAlpha(alphaL)-fPrimAlpha(alpha0))
                deltaAlpha0 = max(deltaAlpha0, tau * (alpha0 - alphaL))
                deltaAlpha0 = min(deltaAlpha0, chi * (alpha0 - alphaL))
                alphaL = alpha0
                alpha0 = alpha0 + deltaAlpha0
            else:
                alphaU = min(alpha0, alphaU)
                if alphaU == alphaL:
                    break
                alphaBar0 = (((alpha0-alphaL)**2)*fPrimAlpha(alphaL)) / (2*(fAlpha(alphaL) - fAlpha(alpha0) + (alpha0 - alphaL)*fPrimAlpha(alphaL)))
                alphaBar0 = max(alphaBar0, alphaL + tau*(alphaU - alphaL))
                alphaBar0 = min(alphaBar0, alphaU - tau*(alphaU - alphaL))
                alpha0 = alphaBar0

            LC = fAlpha(alpha0) >= fAlpha(alphaL) + \
                 (1 - rho) * (alpha0 - alphaL) * fPrimAlpha(alphaL)
            RC = fPrimAlpha(alpha0) <= fAlpha(alphaL) + \
                 rho*(alpha0 - alphaL) * fPrimAlpha(alphaL)
        return alpha0
        
class Broyden(NewtonMethod):
    def __init__(self,problem,x):
        self.problem=problem
        self.gradient=problem.computeGradient()
        H0=self.hessian(self.gradient,x,problem.dimensions)
        self.Q0=nl.inv(H0)
        self.x=x
    def Qstep(self,Qinv,x0,x1):
        deltax=x1-x0
        deltaf=self.gradient(x1)-self.gradient(x0)
        if all(deltax==0) or all(deltaf==0):
            return(False)
        Qinv=Qinv+((deltax-Qinv@deltaf)/(np.transpose(deltax)@Qinv@deltaf))@np.transpose(deltax)*Qinv
        return(Qinv)
    def Broydensolve(self,exactlinesearch=False):
        Q0=self.Q0
        x0=self.x
        while np.any(Q0):
            sk=-Q0@self.gradient(x0)
            if exactlinesearch:
                ak=self.exactLineSearch(x0,sk)
            else:
                ak=self.inexactLineSearch(x0,sk)
            x1=x0+ak*sk
            Q0=self.Qstep(Q0,x0,x1)
            x0=x1
        return(x0)

class Problem:

    def __init__(self, objFunc,dimensions, gradient=None,):
        self.objFunc = objFunc
        self.dimensions = dimensions
        self.gradient = gradient
        if not gradient:
            self.gradient = self.computeGradient()

    def computeGradient(self):
        grad = []
        I = np.identity(self.dimensions)
        h = lambda x: x * 10 ** -8 if x != 0 else 10 ** -8
        grad.append(lambda x: np.array([(self.objFunc(x + I[:, 0]*h(x[0])) - self.objFunc(x))/h(x[0])]))
        for i in range(self.dimensions - 1):
            k = i + 1
            grad.append(lambda x: np.append(grad[k-1](x),(self.objFunc(x + I[:, k]*h(x[k])) - self.objFunc(x))/h(x[k])))
        return grad[self.dimensions-1]


def rosenbrock(x):
    return 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2



#rosenbrock = lambda x: 100*(x[1] - x[0]**2)**2 + (1 - x[0])**2

np.seterr(all='raise')

p = Problem(rosenbrock, 2)



#print(p.get_h()(0.1))

number = p.gradient(np.array([0.1,0.2]))
#print(number)

met = NewtonMethod(p)

print('scipy minimize', minimize(rosenbrock, np.array([5.0,5.0])).x)

#print(met.exactLineSearch(np.array([1,2]), np.array([0.5,0.5])))


steps = met.solve()
print('our newton', steps[-1])

#print(met.inexactLineSearch(np.array([1,2]), np.array([0.5,0.5])))
def rosen2(x, y):
    return 100*(y - x**2)**2 + (1 - x)**2

delta = 0.005
x = np.arange(-0.5, 2.0, delta)
y = np.arange(-1.5, 4.0, delta)
X, Y = np.meshgrid(x, y)
Z = rosen2(X,Y)
plt.contour(X, Y, Z, np.logspace(-0.5,3.5,20,base=10))
step_x = [i[0] for i in steps]
step_y = [i[1] for i in steps]
plt.plot(step_x, step_y, 'o')

print(steps)